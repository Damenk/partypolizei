var NAMESPACE = '/partyPolizei';
module.exports = {
	initialize: function(socket, express, app){
		
		
		app.use("/partypolizei", express.static(__dirname+'/../musicplayer'));
		app.use("/partypolizei/public", express.static(__dirname+'/../public'));
		
		var nsp = socket.of(NAMESPACE);
		nsp.on('connection', function(socket){
			
			socket.on('disconnect', function(){
			});
			
			socket.on("trackchange" , function(data){	
				socket.broadcast.emit("trackchange", data);
			});
			
			socket.on("playback", function(data){
				socket.broadcast.emit("playback",data);
			});
			
			socket.on("ready", function(){
				socket.broadcast.emit("ready");
			});
			
			socket.on("currentTrack", function(data){
				socket.broadcast.emit("currentTrack",data)
			});
			
			socket.on("auto", function(data){
				socket.broadcast.emit("auto", data);
			});
			
			socket.on("reverse", function(data){	
				console.log("reverse: "+ data);
			});
			
			socket.on("forward", function(data){
				socket.broadcast.emit("forward", data);
			});
			
			socket.on("volume", function(data){
				socket.broadcast.emit("volume", data);
			});
			
			socket.on("threshold", function(data){

				socket.broadcast.emit("threshold",data)
			});
			
			socket.on("tracksAvailable", function(data){

				socket.broadcast.emit("playlist",data);
			});
			
			socket.on("ui_volume", function(data){
				socket.broadcast.emit("ui_volume", data);
			});
			
		});
		
	}
	,getAuthors() {
		return ["Jonathan Hammoor", "Fabian Knut", "Felix Kröpelin", "Daniel Menke"]
	}, getTitle() {
		return "PartyPolizei"
	}, getInformation() {
		return "Musik für Ihre Gartenparty - Mit selbstständiger Lautstärkeregulierung"
	}
	
}
	