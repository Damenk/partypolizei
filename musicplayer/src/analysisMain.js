
/* define variables */
var audioContext = null;
var chart = null;
var canvasContext = null;
var rafID = null;
var updateInterval = 200;
var socket = io('/partyPolizei');	


$(document).ready(function(){
	
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
	
	/* grab an audio context */
    audioContext = new AudioContext();

	/* Attempt to get audio input */
    try {
        navigator.getUserMedia = 
        	navigator.getUserMedia ||
        	navigator.webkitGetUserMedia ||
        	navigator.mozGetUserMedia;

		/* ask for an audio input */
        navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream, function(err){
			alert(err);
		});
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }

});



var inputStream = null;

function gotStream(stream) {
    /* Create input from Audiostream */
    inputStream = audioContext.createMediaStreamSource(stream);

	/* Create loudnessProcessor */
    loudnessProcessor = createLoudnessProcessor(audioContext);
	
	/* Create loudness-chart */
	chart = createDbChart();
	
	/* Connect inputstream to loudnessProcessor */
    inputStream.connect(loudnessProcessor);

	/* update the chart view */
	setInterval(function(){chart.updateChart()}, updateInterval); 
	socket.on("disconnect", function(){
		loudnessProcessor.shutdown();
	});

}


