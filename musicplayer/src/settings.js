var fastThreshold = 0;
var leqThreshold;

$(document).ready(function () {

	/*socketIO */
	var socket = io('/partyPolizei');

	/*calibrate button */
	var calibrate = $('#calibrate');

	/*automatic button */
	var auto = $('#auto');

	/*reset button */
	var reset = $('#reset');

	/*variables for calibrating the device */
	var dbCounter = 0;
	var calibrateThreshold = 0;
	var dbSum = 0;

	/* check clients local storage for presaved values */
	/* threshold */
	if (localStorage["threshold"] != "") {
		leqThreshold = parseFloat(localStorage["threshold"]);
		$('#thresholdbox').val(leqThreshold.toFixed(2) + " db");
	} else {
		leqThreshold = 0;
		$('#thresholdbox').val((0.0).toFixed(2) + " db");
	}

	/* automatic */
	if (localStorage["auto"] == "true") {
		$('label[for="' + auto.attr('id') + '"]').addClass("active");
		console.log("auto", localStorage["auto"]);
		console.log("autobtnval ", auto.prop("checked"));
	} else {
		$('label[for="' + auto.attr('id') + '"]').removeClass("active");
	}

	/* function for setting the loudness-threshold */
	var setCalibrateThreshold = function (db) {

		dbSum += db;
		dbCounter++;

		calibrateThreshold = dbSum / dbCounter;

	}

	/*automatic-button handling */
	auto.on("change", function () {
		localStorage["auto"] = auto.prop("checked");
		console.log("autoVal ", auto.prop("checked"));
		socket.emit("auto", {
			"value" : auto.prop('checked')
		})
	});

	/* calibrate-button handling */
	$('#calibrate').on("click", function () {
		var i = 0;
		dbSum = 0;
		dbCounter = 0;
		setCalibrateThreshold(loudnessProcessor.db);
		chart.options.axisY.stripLines[0].value = calibrateThreshold;
		leqThreshold = calibrateThreshold;
		localStorage["threshold"] = calibrateThreshold;

		$('#thresholdbox').val(calibrateThreshold.toFixed(2) + " db");

	});
	
	/* reset-button handling */
	reset.on("click", function () {
		
		/* reset current and local storage values */
		leqThreshold = 0;
		localStorage["threshold"] = 0;

		bufferCount = 0;
		localStorage["bufferCount"] = 0;

		bufferSum = 0;
		localStorage["bufferSum"] = 0;

		dbSum = 0;
		localStorage["dbSum"] = 0;

		localStorage["auto"] = false;
		
		/* disable automatic on raspberry pi */
		$('#thresholdbox').val((0).toFixed(2) + "db");
		socket.emit("auto", {
			"value" : false
		});
		$('label[for="' + auto.attr('id') + '"]').removeClass("active");
		chart.options.axisY.stripLines[0].value = 0;
	});
	
	/* threshold-box handling */
	$('#thresholdbox').on("change", function () {
		leqThreshold = $('#thresholdbox').val()
	});

});
