
/* define variables */
var bufferCount;
var bufferSum;
var leq;
var dbSum;


/* intialize variables with stored values, if there are any. Else initialize with 0 */
if (localStorage["bufferCount"] != "" && ! (isNaN(localStorage["bufferCount"])))  {
	bufferCount = parseInt(localStorage["bufferCount"])
} else {
	bufferCount = 0;
}

if (localStorage["bufferSum"] != "" && ! (isNaN(localStorage["bufferSum"]))) {
	bufferSum = parseInt(localStorage["bufferSum"])
} else {
	bufferSum = 0;
}

if (localStorage["leq"] != "" && ! (isNaN(localStorage["leq"]))) {
	leq = parseFloat(localStorage["leq"])
} else {
	leq = 0;
}

if (localStorage["dbSum"] != "" && ! (isNaN(localStorage["dbSum"]))) {
	dbSum = parseFloat(localStorage["dbSum"])
} else {
	dbSum = 0;
}


/* define start time for threshold barrier */
var thresholdStartTime = Date.now()

/* define start time for leq reset interval */
var leqStartTime = Date.now();


/* function for creating a loudnessprocessor */
function createLoudnessProcessor(audioContext, clipLevel, averaging) {
	
	/* get audioContext scriptprocessor */
	var processor = audioContext.createScriptProcessor(8192);
	
	/* use volumeAudioProcess as audioprocess */
	processor.onaudioprocess = volumeAudioProcess;
	processor.lastClip = 0;
	processor.volume = 0;
	processor.averaging = averaging || 0.00;

	/* connect processor to input */
	processor.connect(audioContext.destination);

	/* function to be called if client disconnects */
	processor.shutdown =
	function () {
		this.disconnect();
		this.onaudioprocess = null;
	};

	return processor;
}

/* define loudnessprocess */
function volumeAudioProcess(event) {
	/* get input buffer */
	var input = event.inputBuffer.getChannelData(0),
	
	/* get input length */
	bufferLength = input.length,
	
	/* intialize variables */
	total = i = 0,
	rms,
	db,
	now = Date.now(),
	elapsed = now - thresholdStartTime,
	elapsedTotal = now -leqStartTime;

	
	/* restart measurement each minute */
	if(elapsedTotal/1000 >60){
		console.log(dbSum, bufferSum, total);
		bufferSum = 0;
		bufferCount = 0;
		dbSum = 0;
		
		leqStartTime = Date.now();
	}
	
	bufferCount++;
	
	/* set localStorage value */
	localStorage["bufferCount"] = bufferCount;
	while (i < bufferLength) {
		total += Math.abs(input[i++]);
	}
	
	//  take the square root of the sum.
	rms = Math.sqrt(total / bufferLength);

	this.db = (20 * Math.log10(rms));

	dbSum += this.db;
	
	/* set localStorage value */
	localStorage["dbSum"] = dbSum;

	bufferSum += Math.pow(10, this.db / 10);
	/* set localStorage value */
	ocalStorage["bufferSum"] = bufferSum;

	this.leq = 10 * Math.log10((1 / bufferCount) * bufferSum);
	/* set localStorage value */
	localStorage["leq"] = this.leq;

	/* notify raspberry pi, if threshold was reached and update client-ui  */
	if (this.leq > leqThreshold) {

		sendThreshold(elapsed, this.db);
		setTimeout(function () {
			$('.threshold-indicator').css('background-color', 'green');

			$('.bluelight').removeClass("brightness");
		}, 500);
		$('.bluelight').addClass("brightness");

	}else{
		$('.bluelight').addClass("brightness");
	}

}

/* sendThreshold function */
function sendThreshold(time, db) {
	if (time > 100) {
		socket.emit("threshold", {
			"value" : db
		});
		thresholdStartTime = Date.now()
	}

}

