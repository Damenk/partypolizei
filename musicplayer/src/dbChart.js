
function createDbChart() {

	/* line colors */
	var fastDbColor = '#9ce994',
	leqDbColor = '#f770fa';
	

	 /* decibel data points for chart */
	var decibelValues = [{
			x : 0,
			y : 0
		}
	]; 
	
	 /* leq data points for chart */
	var leqValues = [{
			x : 0,
			y : 0
		}
	]; 
	
	
	/* configure dbChart */
	var options = {
		height:200,
		backgroundColor : "grey",
		title : {
			fontColor : 'black',
			text : "Lautstärke-Verlauf",

		},
		axisY : {
			
			stripLines:[
			{
				value:leqThreshold,
				thickness:1
			}
			],
			valueFormatString : "0 db",
			labelFontColor : 'black',
			gridColor : "black",
			lineColor : "black",
			tickColor : "black",

		},

		axisX : {
			labelFontColor : 'black',
			gridColor : "black",
			lineColor : "black",
			tickColor : "black",

			labelFormatter : function (e) {
				
				/*callback function to format milliseconds to mm:ss  */
					
					var minutes  = Math.floor(e.value / 1000/60)
					var seconds = e.value/1000%60;
					
					if(seconds==0){
							seconds = seconds+'0';
					}else if(seconds<10){
							seconds= '0'+seconds;
					}
					
					if(minutes<10){
							minutes = '0'+minutes;
					}
					
					return minutes+':'+seconds;
		
				}
			

		},
		toolTip : {
			backgroundColor : "#606060",
			borderColor : "black",
			shared : true
		},
		data : [{
				legendMarkerType : "square",
				markerType : "none",
				markerSize : 10,
				markerColor : fastDbColor,
				type : "line",
				name : 'Fast db',
				legendText : 'fast db',
				showInLegend : true,
				dataPoints : decibelValues,

			}, {
				legendMarkerType : "square",
				markerType : "none",
				markerSize : 10,
				markerColor : leqDbColor,
				type : "line",
				name : 'Leq db',
				legendText : 'Leq db',
				showInLegend : true,
				dataPoints : leqValues,

			}
		],
		legend : {
			cursor : "pointer",
			itemclick : function (e) {
				if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
					e.dataSeries.visible = false;
				} else {
					e.dataSeries.visible = true;
				}
			}

		}

	};

	var chart = new CanvasJS.Chart("chartContainer", options);

	var dataLength = 60; // display two minutes of data
	var then = Date.now();
	chart.updateChart = function (count) {
		count = count || 1;
		// count is number of times loop runs to generate random dataPoints.

		for (var j = 0; j < count; j++) {

			decibelValues.push({

				x : Date.now() - then,
				y : loudnessProcessor.db != null ? Math.floor(loudnessProcessor.db * 100) / 100 : null, // cut float digits and prevent undefined values
				lineColor : loudnessProcessor.db < fastThreshold ? fastDbColor : 'red',
				color : loudnessProcessor.db < fastThreshold ? fastDbColor : 'red',
				

			});
			leqValues.push({

				x : Date.now() - then,
				y : loudnessProcessor.leq != null ? Math.floor(loudnessProcessor.leq * 100) / 100 : null,
				lineColor : loudnessProcessor.leq < leqThreshold ? leqDbColor : 'red',
				color : loudnessProcessor.leq < leqThreshold ? leqDbColor : 'red',

			});
			

		};
		if (decibelValues.length > dataLength || leqValues.length > dataLength) {
			decibelValues.shift();
			leqValues.shift();
		}

		chart.render();

	};

	// generates first set of dataPoints
	chart.updateChart(0);

	return chart;

}
