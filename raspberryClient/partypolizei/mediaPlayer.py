import os
import time
from socketIO_client import SocketIO, BaseNamespace
import pygame
import threading

# initilalize pygame mixer for playpack
mixer = pygame.mixer
mixer.init()

# connect to server via socketIO
socketIO = SocketIO('https://mobile.mt.haw-hamburg.de')

# define namespace
socket = socketIO.define(BaseNamespace, '/partyPolizei')

# initialize variables
tracks = []
auto = False
currentTrack = ""
playback = False

# threaded playback-function which checks iif music is playing. If not the next track is played
def check_playback():
	global currentTrack
	
	# create threaded timer
	threading.Timer(2.0, check_playback).start()
	if mixer.music.get_busy() == 0 and playback == True:
		if currentTrack != len(tracks)-1:
			currentTrack = currentTrack+1
		else:
			currentTrack = 0
		# load next track in playlist	
		mixer.music.load("tracks/"+tracks[currentTrack])	
		
		# tell the client which track will be played
		socket.emit("currentTrack", currentTrack)
		
		# play track
		mixer.music.play()

# start thread		
check_playback()

# list tracks in directory "tracks"
def load_tracks():
	global tracks
	tracks = os.listdir("tracks")

# function for changing the currently playing track via socketIO
def on_trackchange(*args):
	global currentTrack
	global playback
	
	# set current track 
	currentTrack = args[0]['value']
	
	# load and play track
	mixer.music.load("tracks/"+tracks[currentTrack])
	mixer.music.play()
	playback = True	
	
# function to play/pause the playback 	
def on_playback(*args):
	global playback
	print(args[0]['value'])	
	if args[0]['value']==True:
		playback = True
		print("playing")
		mixer.music.unpause()
	else:
		playback = False
		mixer.music.pause()
		
# this function is called when a client connects to the server.
# it notifys the client-UI about the available tracks, the current volume and the currently playing track 
def on_ready():
	load_tracks()
	socket.emit("tracksAvailable", tracks)
	socket.emit("ui_volume", mixer.music.get_volume())
	socket.emit("currentTrack", currentTrack)

# this function is called when the volume threshold of the clients device is reached and decreases the 
# playback volume in steps of 0.001
def on_threshold(*args):

	# get current volume
	actualVolume = mixer.music.get_volume();
	
	# if automatic volume control is activated and the current volume is above the minimum volume,
	# decrease the volume by 0.001
	if auto==True and actualVolume >= 0.1:
		mixer.music.set_volume(mixer.music.get_volume()-0.001)
		socket.emit("ui_volume", mixer.music.get_volume())		
		
	# if the current volume reaches the minimum volume, play a warning signal with full volume
	if mixer.music.get_volume()<0.1:
		mixer.music.set_volume(1.0)
		mixer.music.load("warning/1.mp3")
		mixer.music.play()

# function to activate/deactivate the automatic volume control
def on_auto(*args):
	global auto
	auto = args[0]['value']
	print("automatic: ",auto)

# function to set the volume via SocketIO	
def on_volume(*args):
	mixer.music.set_volume(args[0]['value'])

# define the SocketIO listeners to execute the correct functions	
socket.on('connection', on_ready)
socket.on('ready', on_ready)
socket.on('trackchange', on_trackchange)
socket.on('playback', on_playback)
socket.on('threshold', on_threshold)
socket.on('auto', on_auto)
socket.on('volume', on_volume)


# wait
socketIO.wait()
